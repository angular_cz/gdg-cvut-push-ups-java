package cz.angular.demo.gdgcvut.rest;

import java.util.Collection;

/**
 * Created by vita on 07.12.14.
 */
public class UserInfo {
  private String name;
  private Collection<String> roles;
  private String token;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Collection<String> getRoles() {
    return roles;
  }

  public void setRoles(Collection<String> roles) {
    this.roles = roles;
  }

  public UserInfo() {
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getToken() {
    return token;
  }
}

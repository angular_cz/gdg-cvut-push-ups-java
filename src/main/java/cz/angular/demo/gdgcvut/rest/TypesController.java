package cz.angular.demo.gdgcvut.rest;

import cz.angular.demo.gdgcvut.domain.Type;
import cz.angular.demo.gdgcvut.services.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/")
public class TypesController {

  @Autowired
  TypeRepository typeRepository;

  @RequestMapping(value = "/activity-types", method= RequestMethod.GET)
  public Iterable<Type> getTypes() {
    return typeRepository.findAll();
  }


}
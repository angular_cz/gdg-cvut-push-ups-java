package cz.angular.demo.gdgcvut.rest;

/**
 * Created by vita on 07.12.14.
 */
public class Credentials {

  private String name;
  private String password;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Credentials() {
  }
}
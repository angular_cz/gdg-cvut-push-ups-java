package cz.angular.demo.gdgcvut.rest;

import cz.angular.demo.gdgcvut.TokenFilter;
import cz.angular.demo.gdgcvut.TokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@EnableGlobalMethodSecurity(securedEnabled = true)
public class AuthorizeController {

  @Autowired
  private TokenStore tokenStore;

  @Autowired
  private AuthenticationManager authManager;

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public UserInfo authorize(@RequestBody Credentials creditials) {

    UsernamePasswordAuthenticationToken authenticationToken =
        new UsernamePasswordAuthenticationToken(creditials.getName(), creditials.getPassword());

    Authentication resultOfAuthentication = this.authManager.authenticate(authenticationToken);
    SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);

    String token = tokenStore.saveAndGetToken(authenticationToken);

    UserInfo userInfo = new UserInfo();
    userInfo.setName(resultOfAuthentication.getName());

    List<String> roles = getRoles(resultOfAuthentication);
    userInfo.setRoles(roles);
    userInfo.setToken(token);

    return userInfo;
  }

  @RequestMapping(value = "/logout", method = RequestMethod.POST)
  public void getPublic(@RequestHeader(TokenFilter.TOKEN_HEADER) String token) {
    tokenStore.removeToken(token);
  }

  private List<String> getRoles(Authentication resultOfAuthentication) {
    ArrayList<String> roles = new ArrayList<String>();

    Collection<? extends GrantedAuthority> authorities = resultOfAuthentication.getAuthorities();
    for (GrantedAuthority authority : authorities) {
      roles.add(authority.getAuthority());
    }

    return roles;
  }
}

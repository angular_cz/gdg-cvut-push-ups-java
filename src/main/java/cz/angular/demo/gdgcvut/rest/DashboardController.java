package cz.angular.demo.gdgcvut.rest;

import org.eclipse.jetty.util.ajax.JSON;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vita on 30.04.16.
 */
@RestController
public class DashboardController {

  @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
  public Object getDashboard() {
    return JSON.parse(getFakeJson());
  }

  public String getFakeJson() {
    return "  {\n" +
        "          \"rating\": 10,\n" +
        "          \"today\": [\n" +
        "            {\n" +
        "              \"activity\": \"Pushups\",\n" +
        "              \"count\": 33\n" +
        "            },\n" +
        "            {\n" +
        "              \"activity\": \"Squats\",\n" +
        "              \"count\": 20\n" +
        "            }\n" +
        "          ],\n" +
        "          \"month\": {\n" +
        "            \"activities\": [\n" +
        "              {\n" +
        "                \"activity\": \"Pushups\",\n" +
        "                \"count\": 330\n" +
        "              },\n" +
        "              {\n" +
        "                \"activity\": \"Squats\",\n" +
        "                \"count\": 200\n" +
        "              }\n" +
        "            ],\n" +
        "            \"daily\": [\n" +
        "              {\n" +
        "                \"key\": \"Pushups\",\n" +
        "                \"values\": [\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-01T07:02Z\",\n" +
        "                    \"y\": 5\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-02T07:02Z\",\n" +
        "                    \"y\": 10\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-03T07:02Z\",\n" +
        "                    \"y\": 1\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-04T07:02Z\",\n" +
        "                    \"y\": 10\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-05T07:02Z\",\n" +
        "                    \"y\": 7\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-06T07:02Z\",\n" +
        "                    \"y\": 10\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-06T07:02Z\",\n" +
        "                    \"y\": 5\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-07T07:02Z\",\n" +
        "                    \"y\": 10\n" +
        "                  }\n" +
        "                ]\n" +
        "              },\n" +
        "              {\n" +
        "                \"key\": \"Squats\",\n" +
        "                \"values\": [\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-01T07:02Z\",\n" +
        "                    \"y\": 5\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-02T07:02Z\",\n" +
        "                    \"y\": 10\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-03T07:02Z\",\n" +
        "                    \"y\": 1\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-04T07:02Z\",\n" +
        "                    \"y\": 10\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-05T07:02Z\",\n" +
        "                    \"y\": 7\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-06T07:02Z\",\n" +
        "                    \"y\": 1\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-06T07:02Z\",\n" +
        "                    \"y\": 5\n" +
        "                  },\n" +
        "                  {\n" +
        "                    \"x\": \"2016-03-07T07:02Z\",\n" +
        "                    \"y\": 10\n" +
        "                  }\n" +
        "                ]\n" +
        "              }\n" +
        "            ]\n" +
        "          }\n" +
        "        }";
  }

}

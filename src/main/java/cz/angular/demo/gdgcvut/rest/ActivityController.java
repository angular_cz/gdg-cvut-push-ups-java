package cz.angular.demo.gdgcvut.rest;

/**
 * Created by vita on 30.04.16.
 */

import cz.angular.demo.gdgcvut.domain.User;
import cz.angular.demo.gdgcvut.services.ActivityRepository;
import cz.angular.demo.gdgcvut.domain.Activity;
import cz.angular.demo.gdgcvut.domain.Type;
import cz.angular.demo.gdgcvut.services.TypeRepository;
import cz.angular.demo.gdgcvut.services.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityController {

  @Autowired
  ActivityRepository activityRepository;

  @Autowired
  TypeRepository typeRepository;

  @Autowired
  UserRepository userRepository;

  @RequestMapping(value = "/activities", method = RequestMethod.GET)
  public Page<Activity> getActivities(@RequestParam(value = "page", defaultValue = "1") int page,
                                      @RequestParam(value = "number", defaultValue = "10") int number) {

    Sort.Order order = new Sort.Order(Sort.Direction.DESC, "date");

    PageRequest pageable = new PageRequest(page - 1, number, new Sort(order));

    return activityRepository.findAllByUser(pageable, getLoggedUser());
  }

  @RequestMapping(value = "/activities", method = RequestMethod.POST)
  public Activity create(@RequestBody Activity activityDTO) {

    Type type = typeRepository.findOne(activityDTO.getType().getId());
    Activity activity = new Activity(getLoggedUser(), type, activityDTO.getCount(), activityDTO.getDate());

    return activityRepository.save(activity);
  }

  @RequestMapping(value = "/activities/{id}", method = RequestMethod.GET)
  public ResponseEntity<Activity> getOne(@PathVariable(value = "id") Long id) {
    Activity activity = activityRepository.findOne(id);
    if(getLoggedUser() != activity.getUser()) {
      return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

    return new ResponseEntity<Activity>(activity, HttpStatus.OK);
  }

  @RequestMapping(value = "/activities/{id}", method = RequestMethod.DELETE)
  public ResponseEntity delete(@PathVariable(value = "id") Long id) {

    Activity activity = activityRepository.findOne(id);

    if(getLoggedUser() != activity.getUser()) {
      return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

    activityRepository.delete(activity);

    return new ResponseEntity(HttpStatus.OK);
  }

  @RequestMapping(value = "/activities/{id}", method = RequestMethod.POST)
  public ResponseEntity save(@PathVariable(value = "id") Long id, @RequestBody Activity activityDTO) {

    Type type = typeRepository.findOne(activityDTO.getType().getId());

    Activity activity = activityRepository.findOne(id);

    if(getLoggedUser() != activity.getUser()) {
      return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

    activity.setCount(activityDTO.getCount());
    activity.setDate(activityDTO.getDate());
    activity.setType(type);

    activityRepository.save(activity);

    return new ResponseEntity(HttpStatus.OK);
  }

  private User getLoggedUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return userRepository.findOneByName(authentication.getName());
  }
}

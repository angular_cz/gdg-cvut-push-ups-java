package cz.angular.demo.gdgcvut.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by vita on 30.04.16.
 */
@Entity
public class Activity {

  @Id
  @GeneratedValue
  private Long id;

  @JsonIgnore
  @ManyToOne(optional = false)
  private User user;

  @ManyToOne(optional=false)
  private Type type;

  @Column
  private Long count;

  @Column
  private Date date;

  public Activity() {
  }

  public Activity(User user, Type type, Long count, Date date) {
    this.user = user;
    this.type = type;
    this.count = count;
    this.date = date;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public Long getCount() {
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}

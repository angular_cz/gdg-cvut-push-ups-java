package cz.angular.demo.gdgcvut.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by vita on 30.04.16.
 */
@Entity
public class Type {

  @Id
  @GeneratedValue
  private Long id;

  @Column
  private String name;

  public Type() {
  }

  public Type(String name) {
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}

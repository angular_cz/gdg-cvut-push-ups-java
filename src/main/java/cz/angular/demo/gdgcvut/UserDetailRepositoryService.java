package cz.angular.demo.gdgcvut;

import cz.angular.demo.gdgcvut.domain.User;
import cz.angular.demo.gdgcvut.services.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserDetailRepositoryService implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username)
      throws UsernameNotFoundException {

    User user = userRepository.findOneByName(username);

    if(user==null) {throw new UsernameNotFoundException("No such user: " + username);
    }

    boolean accountNonExpired = true;
    boolean credentialsNonExpired = true;
    boolean accountNonLocked = true;
    boolean accountIsEnabled = true;
    return new org.springframework.security.core.userdetails.User(
        user.getName(),
        user.getPassword(),
        accountIsEnabled,
        accountNonExpired,
        credentialsNonExpired,
        accountNonLocked,
        AuthorityUtils.createAuthorityList("ROLE_USER"));
  }
}
package cz.angular.demo.gdgcvut;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by vita on 01.12.14.
 */

@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  private TokenStore tokenStore;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint())
        .and()

        .anonymous().and()
        .servletApi().and()
        .headers().cacheControl().and()
        .authorizeRequests()

        //allow anonymous POSTs to login
        .antMatchers(HttpMethod.POST, "/login").permitAll()
        .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
        .anyRequest().authenticated().and()
        //TODO currently as controller action, but it could be logout handler
        .logout()
        .logoutUrl("/logout-spring")

        .and()
        .csrf().disable();

    http.addFilterBefore(new TokenFilter(tokenStore, authenticationManager()), BasicAuthenticationFilter.class);
  }

  @Bean
  protected AuthenticationEntryPoint unauthorizedEntryPoint() {
    return new AuthenticationEntryPoint() {
      @Override
      public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
      }
    };
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth, UserDetailRepositoryService userDetailRepositoryService) throws Exception {
    auth.userDetailsService(userDetailRepositoryService);
  }

}
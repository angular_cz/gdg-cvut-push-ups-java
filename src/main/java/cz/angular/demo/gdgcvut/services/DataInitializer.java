package cz.angular.demo.gdgcvut.services;

import cz.angular.demo.gdgcvut.domain.User;
import cz.angular.demo.gdgcvut.domain.Activity;
import cz.angular.demo.gdgcvut.domain.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Date;

/**
 * Created by vita on 30.04.16.
 */
@Component
public class DataInitializer {

  @Autowired
  ActivityRepository activityRepository;

  @Autowired
  TypeRepository typeRepository;

  @Autowired
  UserRepository userRepository;

  @PostConstruct
  public void initialize() {

    User user = new User("user1", "pass");
    User user2 = new User("user2", "pass");

    userRepository.save(user);
    userRepository.save(user2);

    Type pushups = new Type("Pushups");
    typeRepository.save(pushups);
    typeRepository.save(new Type("Burpies"));
    typeRepository.save(new Type("Squats"));

    activityRepository.save(new Activity(user, pushups, 10L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 11L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 12L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 13L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 14L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 15L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 16L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 17L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 19L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 20L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 21L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user, pushups, 22L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user2, pushups, 1L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user2, pushups, 2L, Date.valueOf("1985-05-07")));
    activityRepository.save(new Activity(user2, pushups, 3L, Date.valueOf("1985-05-07")));

  }
}

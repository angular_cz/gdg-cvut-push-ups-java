package cz.angular.demo.gdgcvut.services;

import cz.angular.demo.gdgcvut.domain.User;
import cz.angular.demo.gdgcvut.domain.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vita on 30.04.16.
 */
public interface ActivityRepository  extends CrudRepository<Activity, Long> {

  Page<Activity> findAll(Pageable pageable);

  Activity findOneByIdAndUser(Long id, User user);

  Page<Activity> findAllByUser(Pageable pageable, User user);
}

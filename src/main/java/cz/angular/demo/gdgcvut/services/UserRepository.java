package cz.angular.demo.gdgcvut.services;

import cz.angular.demo.gdgcvut.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vita on 30.04.16.
 */
public interface UserRepository extends CrudRepository<User, Long> {

  User findOneByName(String name);
}

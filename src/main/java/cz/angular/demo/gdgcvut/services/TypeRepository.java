package cz.angular.demo.gdgcvut.services;

import cz.angular.demo.gdgcvut.domain.Type;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vita on 30.04.16.
 */
public interface TypeRepository extends CrudRepository<Type, Long> {

}

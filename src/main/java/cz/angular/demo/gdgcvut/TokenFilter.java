package cz.angular.demo.gdgcvut;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by vita on 07.12.14.
 */

public class TokenFilter extends OncePerRequestFilter
  {

    public static final String TOKEN_HEADER = "X-Auth-Token";
    private final TokenStore tokenStore;
    private final AuthenticationManager authManager;

    public TokenFilter(TokenStore tokenStore, AuthenticationManager authManager) {
      this.tokenStore = tokenStore;
      this.authManager = authManager;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {

      String token = extractAuthTokenFromRequest(request);
      Authentication forToken = tokenStore.getForToken(token);

      if (forToken != null) {
        Authentication resultOfAuthentication = this.authManager.authenticate(forToken);
        SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
      }
      filterChain.doFilter(request, response);
    }

    private String extractAuthTokenFromRequest(HttpServletRequest httpRequest)
    {
		/* Get token from header */
      String authToken = httpRequest.getHeader(TOKEN_HEADER);

		/* If token not found get it from request parameter */
      if (authToken == null) {
        authToken = httpRequest.getParameter("token");
      }

      return authToken;
    }


}
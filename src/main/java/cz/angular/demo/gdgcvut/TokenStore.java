package cz.angular.demo.gdgcvut;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vita on 07.12.14.
 */
@Component
public class TokenStore {
  Map<String, Authentication> auths = new HashMap<String, Authentication>();

  public String saveAndGetToken(Authentication auth) {
    String token = getToken();
    auths.put(token, auth);

    return token;
  }

  public Authentication getForToken(String token) {
    return auths.get(token);
  }

  private String getToken() {

    MessageDigest m = null;
    try {
      m = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }

    SecureRandom secureRandom = new SecureRandom();

    String s = Long.valueOf(secureRandom.nextLong()).toString();
    m.update(s.getBytes(),0,s.length());
    return new BigInteger(1,m.digest()).toString(16);
  }

  public void removeToken(String token) {
    auths.remove(token);
  }
}
